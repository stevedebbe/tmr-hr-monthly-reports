'
' VBA code and functions to support the HR Reporting UiPath Bot
' The purpose of the code in this file is to populate two email templates
' One for the GM Commentary Email monthly GM Dashboard commentary and the other
' for the Local Report emails that are sent to each local business area.
' Author: Matthew Hodgson (matt.hodgson@blackbook.ai)
' Date: 31 August 2018
'
Option Explicit

Function GMCommentaryEmail(inMailTemplateFilePath As String, inGMDashBoardFilePath As String)
'Populates a GM Commentary Email template with the latest GM Dashboard Excel Report as an attachment
'Updates the due date in the email body and subject line as today's date + 7 days
'Saves a copy of the prepared email in the \temp folder and displays it on the screen ready to send
    Dim objOL As Object
    Dim Msg As Object
    Set objOL = CreateObject("Outlook.Application")
    Application.ScreenUpdating = False
    Dim thisFile As String
    Dim EmailSubject As String
    EmailSubject = "ACTION required: GM Dashboard - HR, SWIP Commentary + Recruitment deadlines - DUE COB " + Format(Date+7, "dddd, d mmmm yyyy")
    'Get attachment
    Dim strPath As String, strName As String, strFilter As String, strFile As String
    thisFile = inMailTemplateFilePath & "gm-commentary-email-template.msg"
    Set Msg = objOL.Session.OpenSharedItem(thisFile)
    With Msg
        .Subject = EmailSubject
        .Attachments.Add inGMDashBoardFilePath
        .HTMLBody = Replace(.HTMLBody, "[insert-due-date]" , Format(Date+7, "dddd, d mmmm yyyy"))
    End With
    Dim tempEmailFile As String
    tempEmailFile = inMailTemplateFilePath & "temp\" & "gm-commentary-email-template.msg"
    Msg.SaveAs tempEmailFile
    Msg.Close 1
    Set Msg = objOL.Session.OpenSharedItem(tempEmailFile)
    With Msg
        '------------------------------------------------------------
        ' Change to .send if you want the email to send automatically
        '------------------------------------------------------------
        .display
        '------------------------------------------------------------
    End With
    Set objOL = Nothing
    Set Msg = Nothing
    Application.ScreenUpdating = True
End Function

Function LocalReportEmail(inMailTemplateFilePath As String, businessAreaName as String, reportDirectory As String, PDANameReportFileName As String)
'Populates a HR Monthly Spotlight Email template with data from the recently completed GM Dashboard excel file
'Updates the values in the email body with data from the excel file.  There are text identifiers in the body
'of the email which the VBA replaces with the values from the excel file.   
'Saves copies of the prepared emails in the \temp folder and displays them on the screen ready to send
    Dim objOL As Object
    Dim Msg As Object
    Set objOL = CreateObject("Outlook.Application")
    Application.ScreenUpdating = False
    Dim thisFile As String
    Dim mainWB As Workbook
    set mainWB = ActiveWorkbook 
    Dim ERLNamesRange As Range, PDANamesRange As Range
    Dim ERLNames As String,  PDANames As String
    Dim PDAPercentage As String
    Dim EmployeesRecLeave As String, AcceleratePercentage As String
    'Get Excess Rec Leave values
    With ActiveWorkbook.Sheets("Excess Rec Leave")
        .rows("3:3").AutoFilter
        Dim rows As String
        rows = .UsedRange.rows.Count
        .rows("3:3").AutoFilter
        .Range("$A$3:$R$" + rows).AutoFilter Field:=2, Criteria1:=businessAreaName
        On Error GoTo SkipERLNames:
        Set ERLNamesRange = .Range("F4:F" + rows).SpecialCells(xlCellTypeVisible)
        EmployeesRecLeave = ERLNamesRange.Cells.Count
        Dim cell As Range
        Dim firstName As Boolean
        firstName = True
        For Each cell In ERLNamesRange
            If firstName = True Then
                ERLNames = "<ul><li>" + cell.Value + "</li>"
                firstName = False
            Else
                ERLNames = ERLNames + "<li>" + cell.Value + "</li>"
            End If
        Next cell
        ERLNames = ERLNames + "</ul>"
BackToERLNames:
    End With
    'Retrieve names from the Employee Performance Plans report
    Dim PDANameWB As Workbook
    Set PDANameWB = Workbooks.Open(reportDirectory + PDANameReportFileName)
    Dim sht As Worksheet
    Set sht = PDANameWB.Worksheets(1)
    With sht
        .rows("2:2").AutoFilter
        Dim rws As String
        rws = .UsedRange.rows.Count
        .rows("2:2").AutoFilter
        .Range("$A$2:$G$" + rws).AutoFilter Field:=1, Criteria1:=businessAreaName
        .Range("$A$2:$G$" + rws).AutoFilter Field:=7, Criteria1:=1
    On Error GoTo SkipPDANames:
    Set PDANamesRange = .Range("C3:C" + rws).SpecialCells(xlCellTypeVisible)
    Dim cellx As Range
    firstName = True
    For Each cellx In PDANamesRange
        If firstName = True Then
            PDANames = "<ul><li>" + cellx.Value + "</li>"
            firstName = False
        Else
            PDANames = PDANames + "<li>" + cellx.Value + "</li>"
        End If
    Next cellx
    PDANames = PDANames + "</ul>"
BackToPDANames:
    PDANameWB.Close SaveChanges:=False
    End With
    'Get Accelerate average % value
    AcceleratePercentage = WorksheetFunction.Index(mainWB.Sheets("Accelerate").Range("I:I"), WorksheetFunction.Match(businessAreaName, _
    mainWB.Sheets("Accelerate").Range("A:A"), 0), 0)
    If AcceleratePercentage > 0.99 And AcceleratePercentage < 1 Then
        AcceleratePercentage = 0.99
    End If
    'Get PDA % Complete
    PDAPercentage = WorksheetFunction.Index(Sheets("PDA Stats").Range("B:B"), WorksheetFunction.Match(businessAreaName, _
    Sheets("PDA Stats").Range("A:A"), 0), 0)
    If PDAPercentage > 0.99 And PDAPercentage < 1 Then
        PDAPercentage = 0.99
    End If
    thisFile = inMailTemplateFilePath & businessAreaName & " monthly-hr-spotlight.msg"
            Set Msg = objOL.Session.OpenSharedItem(thisFile)
            With Msg
                .HTMLBody = Replace(.HTMLBody, "", "")
            End With
            Msg.HTMLBody = Replace(Msg.HTMLBody, "%PDA%", Format(PDAPercentage, "0%"))
            Msg.HTMLBody = Replace(Msg.HTMLBody, "%PDANAMES%", PDANames)
            Msg.HTMLBody = Replace(Msg.HTMLBody, "%ERL%", EmployeesRecLeave)
            Msg.HTMLBody = Replace(Msg.HTMLBody, "%ERLNAMES%", ERLNames)
            Msg.HTMLBody = Replace(Msg.HTMLBody, "%APOA%", Format(AcceleratePercentage, "0%"))
            Dim tempEmailFile As String
            tempEmailFile = inMailTemplateFilePath & "temp\" & businessAreaName & " monthly-hr-spotlight.msg"
            Msg.SaveAs tempEmailFile
            Msg.Close 1
            Set Msg = objOL.Session.OpenSharedItem(tempEmailFile)
            With Msg
                '------------------------------------------------------------
                ' Change to .send if you want the email to send automatically
                '------------------------------------------------------------
                .display
                '------------------------------------------------------------
            End With
    Set objOL = Nothing
    Set Msg = Nothing
    Application.ScreenUpdating = True
    Exit Function
SkipPDANames:
    PDANames = ""
    GoTo BackToPDANames:
SkipERLNames:
    ERLNames = ""
    EmployeesRecLeave = "0"
    GoTo BackToERLNames:
End Function