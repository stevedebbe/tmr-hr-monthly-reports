'
' VBA code and functions to support the HR Reporting UiPath Bot
' The purpose of this code is to consolidate the Excel reports downloaded from SAP and Accelerate
' into the consolidated monthly GM Dashboard Excel file.
' Author: Matthew Hodgson (matt.hodgson@blackbook.ai)
' Date: 31 August 2018
'
Option Explicit

Function Main(inFilePath As String, inFileName As String, reportDirectory As String) As String
'inFilePath: Folder directory containing the Report Template excel file
'inFileName: Name of the Report Template excel file
    'Disable screen updating for speed
    Application.ScreenUpdating = False
    'Create the main template file to populate
    Dim ErrorMessage As String
    Dim SheetsProcessed As Boolean
    Dim mainWB As Workbook
    Set mainWB = CopyTemplateFile(inFilePath, inFileName)
    'Directory for downloaded reports and report file names
    Dim report_ExcessRecLeave As String, report_ApproachExcessRecLeave As String
    Dim report_Accelerate As String, report_EPP As String, report_UnplannedLeave As String
    Dim report_ExcessSickLeave As String, report_Gender As String
    '------------------------------------------------------------------------------------------------------
    ' - If the downloaded reports directory or the filename of a report changes, update the values below
    '------------------------------------------------------------------------------------------------------
    report_ExcessRecLeave = "R1205 Excess Recreation Leave Balance (Excess by Employee).xls"
    report_ApproachExcessRecLeave = "R1205 Excess Recreation Leave Balance (Approaching by Employee).xls"
    report_Accelerate = "1_snapshot_of__compliant_with_mandatory_courses_report.csv"
    report_EPP = "r1106 Employee Performance Plans.xls"
    report_UnplannedLeave = "R1201 All Leave Taken by Category.xls"
    report_ExcessSickLeave = "R1206 Excessive Sick Leave Taken (Previous Month).xls"
    report_Gender = "R1103 Employee Detail Report.xls"
    '------------------------------------------------------------------------------------------------------
    'Populate each sheet in the main template with the downloaded reports from SAP/Accelerate
    ' - If sheet names in the Report Template excel file change, update the relevant name in brackets below
    '------------------------------------------------------------------------------------------------------
    While ErrorMessage = "" AND SheetsProcessed = False
    'Continue processing sheets unless an error is encountered'
        ErrorMessage = PrepareSheet_RecLeave(mainWB, reportDirectory + report_ApproachExcessRecLeave,"Approaching Excess Rec")
        ErrorMessage = PrepareSheet_RecLeave(mainWB, reportDirectory + report_ExcessRecLeave,"Excess Rec Leave")
        ErrorMessage = PrepareSheet_Accelerate(mainWB, reportDirectory, report_Accelerate,"Accelerate")
        ErrorMessage = PrepareSheet_EPP(mainWB, reportDirectory + "\" + report_EPP,"PDA Stats")
        ErrorMessage = PrepareSheet_UnplannedLeave(mainWB, reportDirectory + report_UnplannedLeave,"Unplanned leave")
        ErrorMessage = PrepareSheet_ExcessSickLeave(mainWB, reportDirectory + report_ExcessSickLeave,"Excess Sick")
        ErrorMessage = PrepareSheet_Gender(mainWB, reportDirectory + report_Gender,"Gender")
        SheetsProcessed = True
    wend
    If Not ErrorMessage = "" Then
        Main = ErrorMessage
    End If
    '------------------------------------------------------------------------------------------------------
    'Turn screen updating back on
    Application.ScreenUpdating = True
    MainWB.Sheets(1).Activate
End Function

Function PrepareSheet_Gender(mainWB As Workbook, inputFile As String, templateSheet As String) As String
'Prepares the data in the Excess Rec Leave and adds it to the GM Dashboard monthly report template.
    'Data workbook variables
    Dim dataWB As Workbook
    Dim dataSht As Worksheet
    Dim dataRow As Integer, dataLastRow As Integer
    Dim businessAreaName As String
    On Error GoTo ErrorHandler:
    Application.DisplayAlerts = False
    Set dataWB = Workbooks.Open(inputFile)
    Application.DisplayAlerts = True
    Set dataSht = dataWB.Sheets(1)
    Call Unmerge_And_AutoFit_All(dataSht.Name)
    'Main template workbook variables
    Dim mainSht As Worksheet
    Dim mainRng As Range
    Dim mainRow As Integer, mainLastRow As Integer
    Set mainSht = mainWB.Sheets(templateSheet)
    'Get the number of rows in the main template sheet
    mainLastRow = mainSht.UsedRange.Rows.Count
    If mainLastRow < 3 Then
        mainLastRow = 3
    Else
    End If
    'Delete any existing data in the main template sheet
    Set mainRng = mainSht.Range("A3:D" & mainLastRow)
    mainRng.EntireRow.Delete
    mainRow = 3
    'Loop through data sheet and sum totals
    dataRow = 3
    dataLastRow = dataSht.UsedRange.Rows.Count
    Dim prevBusinessAreaName As String
    'Get column numbers for the data input sheet
    Dim col_OrgUnits As Integer
    Dim col_Male As Integer
    Dim col_Female As Integer
    Dim col_Unknown As Integer
    col_OrgUnits = WorksheetFunction.Match("Org Unit Hier Lvl 4", dataSht.Rows(2), 0)
    col_Male = WorksheetFunction.Match("Male", dataSht.Rows(2), 0)
    col_Female = WorksheetFunction.Match("Female", dataSht.Rows(2), 0)
    On Error GoTo SetValueTo0:
    col_Unknown = WorksheetFunction.Match("Unknown", dataSht.Rows(2), 0)
    GoTo ContinueFunction:
SetValueTo0:
    col_Unknown = 10
ContinueFunction:
    Dim val_male As Long, val_female As Long, val_unknown As Long
    Dim SPP_Done As Boolean
    SPP_Done = False
    'Loop through each row in the data sheet and retrieve the values from each column in the data sheet
    '[# Emps with Excess Sick][HR Headcount][% with Excess Sick]
    Do While dataRow <= dataLastRow
        'If we've already processed Strategic Planning & Performance, don't process it again, instead,
        'replace the text in this column, with the text in the adjacent column to enable us loop through
        'Org Unit Hier Level 5
        If prevBusinessAreaName = "Strategic Planning & Performance" Then
            SPP_Done = True 'Indicates that we've already processed Strategic Planning & Performance
            dataRow = dataRow - 1 'Go up one row as we need to reprocess the line, but process it as org level 5
        End If
        'Get the default business area name
        businessAreaName = dataSht.Cells(dataRow, col_OrgUnits)
        'Check if we need to get the
        If businessAreaName = "Strategic Planning & Performance" And SPP_Done = True Then
            dataSht.Cells(dataRow, col_OrgUnits).value = dataSht.Cells(dataRow, col_OrgUnits + 1).value
            businessAreaName = dataSht.Cells(dataRow, col_OrgUnits)
        End If
        'If the current row businessAreaName is not the same previous row businessAreaName, then process the row
        If Not businessAreaName = prevBusinessAreaName Then
            With dataSht
                'Retrieve the values we need from the data sheet
                val_male = WorksheetFunction.SumIf(dataSht.Columns(col_OrgUnits), businessAreaName, dataSht.Columns(col_Male))
                val_female = WorksheetFunction.SumIf(dataSht.Columns(col_OrgUnits), businessAreaName, dataSht.Columns(col_Female))
                val_unknown = WorksheetFunction.SumIf(dataSht.Columns(col_OrgUnits), businessAreaName, dataSht.Columns(col_Unknown))
            End With
            With mainSht
                'Insert the values we just retrieved into the main workbook template
                .Range("A" & mainRow).value = businessAreaName
                .Range("C" & mainRow).value = val_male
                .Range("B" & mainRow).value = val_female
                .Range("D" & mainRow).value = val_unknown
                'Colour the cells according to the process doc requirements
                .Range("A" & mainRow).Interior.Color = RGB(198, 196, 196)
                If Not mainRow Mod 2 = 0 Then 'If odd numbered row, set cell colour to Blue
                    .Range("B" & mainRow & ":D" & mainRow).Interior.Color = RGB(221, 235, 247)
                Else: End If
                'Apply a border to the new data
                With .Range("A" & mainRow & ":D" & mainRow).Borders
                    .LineStyle = xlContinuous
                    .Color = RGB(166, 166, 166)
                    .Weight = xlMedium
                End With
            End With
            mainRow = mainRow + 1 'Increment the main workbook row count
        Else 'We don't need to process this row, so clear values and move to next iteration
            val_male = 0
            val_female = 0
            val_unknown = 0
            GoTo ContinueDo:
        End If
ContinueDo:
        prevBusinessAreaName = businessAreaName 'Set prevBusinessAreaName to the businessAreaName we just processed
        dataRow = dataRow + 1                   'Increment the counter
    Loop
    'Close the file
    dataWB.Close SaveChanges:=False
    Call Unmerge_And_AutoFit_All(mainSht.Name)
Exit Function
ErrorHandler:
    Application.ScreenUpdating = True
    Application.DisplayAlerts = True
    PrepareSheet_Gender = "ERROR: Failed to process " & inputFile
End Function

Function PrepareSheet_ExcessSickLeave(mainWB As Workbook, inputFile As String, templateSheet As String) As String
'Prepares the data in the Excess Rec Leave and adds it to the GM Dashboard monthly report template.
    
    'Data workbook variables
    Dim dataWB As Workbook
    Dim dataSht As Worksheet
    Dim dataRow As Integer, dataLastRow As Integer
    Dim businessAreaName As String

    On Error GoTo ErrorHandler:
    Application.DisplayAlerts = False
    Set dataWB = Workbooks.Open(inputFile)
    Application.DisplayAlerts = True

    Set dataSht = dataWB.Sheets(1)
    Call Unmerge_And_AutoFit_All(dataSht.Name)

    'Main template workbook variables
    Dim mainSht As Worksheet
    Dim mainRng As Range
    Dim mainRow As Integer, mainLastRow As Integer
    Set mainSht = mainWB.Sheets(templateSheet)
    
    'Get the number of rows in the main template sheet
    mainLastRow = mainSht.UsedRange.Rows.Count
    If mainLastRow < 4 Then
        mainLastRow = 4
    Else
    End If
    
    'Delete any existing data in the main template sheet
    Set mainRng = mainSht.Range("A4:D" & mainLastRow)
    mainRng.EntireRow.Delete
    mainRow = 4
    
    'Loop through data sheet and sum totals
    dataRow = 4
    dataLastRow = dataSht.UsedRange.Rows.Count
    Dim prevBusinessAreaName As String

    'Get column numbers for the data input sheet
    Dim col_EmpExcessSick As Integer
    Dim col_HRHeadCount As Integer
    Dim col_PercentExcessSick As Integer
    Dim col_OrgUnits As Integer
    col_OrgUnits = WorksheetFunction.Match("Org Unit Hier Lvl 4", dataSht.Rows(3), 0)
    col_EmpExcessSick = WorksheetFunction.Match("# Emps with Excess Sick", dataSht.Rows(2), 0)
    col_HRHeadCount = WorksheetFunction.Match("HR Headcount", dataSht.Rows(2), 0)
    col_PercentExcessSick = WorksheetFunction.Match("% with Excess Sick", dataSht.Rows(2), 0)
    Dim EmpExcessSick As Long
    Dim HRHeadCount As Long
    Dim PercentExcessSick As Double

    Dim SPP_Done As Boolean
    SPP_Done = False
    
    'Copy headers from the data sheet to the main template sheet
    dataSht.Range("E1:G3").Copy
    mainSht.Range("B1:D3").PasteSpecial xlPasteAll

    'Loop through each row in the data sheet and retrieve the values from each column in the data sheet
    '[# Emps with Excess Sick][HR Headcount][% with Excess Sick]
    Do While dataRow <= dataLastRow
        'If we've already processed Strategic Planning & Performance, don't process it again, instead,
        'replace the text in this column, with the text in the adjacent column to enable us loop through
        'Org Unit Hier Level 5
        If prevBusinessAreaName = "Strategic Planning & Performance" Then
            SPP_Done = True 'Indicates that we've already processed Strategic Planning & Performance
            dataRow = dataRow - 1 'Go up one row as we need to reprocess the line, but process it as org level 5
        End If
        
        'Get the default business area name
        businessAreaName = dataSht.Cells(dataRow, col_OrgUnits)
        
        'Check if we need to get the
        If businessAreaName = "Strategic Planning & Performance" And SPP_Done = True Then
            dataSht.Cells(dataRow, col_OrgUnits).value = dataSht.Cells(dataRow, col_OrgUnits + 1).value
            businessAreaName = dataSht.Cells(dataRow, col_OrgUnits)
        End If

        'If the current row businessAreaName is not the same previous row businessAreaName, then process the row
        If Not businessAreaName = prevBusinessAreaName Then
            With dataSht
                'Retrieve the values we need from the data sheet
                EmpExcessSick = WorksheetFunction.SumIf(dataSht.Columns(col_OrgUnits), businessAreaName, dataSht.Columns(col_EmpExcessSick))
                HRHeadCount = WorksheetFunction.SumIf(dataSht.Columns(col_OrgUnits), businessAreaName, dataSht.Columns(col_HRHeadCount))
                PercentExcessSick = EmpExcessSick / HRHeadCount
            End With
            With mainSht
                'Insert the values we just retrieved into the main workbook template
                .Range("A" & mainRow).value = businessAreaName
                .Range("B" & mainRow).value = EmpExcessSick
                .Range("C" & mainRow).value = HRHeadCount
                .Range("D" & mainRow).value = Format(PercentExcessSick, "0.00%")
                'Colour the cells according to the process doc requirements
                .Range("A" & mainRow).Interior.Color = RGB(198, 196, 196)
                If Not mainRow Mod 2 = 0 Then 'If odd numbered row, set cell colour to Blue
                    .Range("B" & mainRow & ":D" & mainRow).Interior.Color = RGB(221, 235, 247)
                Else: End If
                'Apply a border to the new data
                With .Range("A" & mainRow & ":D" & mainRow).Borders
                    .LineStyle = xlContinuous
                    .Color = RGB(166, 166, 166)
                    .Weight = xlMedium
                End With
            End With
            mainRow = mainRow + 1 'Increment the main workbook row count
        Else 'We don't need to process this row, so clear values and move to next iteration
            EmpExcessSick = 0
            HRHeadCount = 0
            PercentExcessSick = 0
            GoTo ContinueDo:
        End If
ContinueDo:
        prevBusinessAreaName = businessAreaName 'Set prevBusinessAreaName to the businessAreaName we just processed
        dataRow = dataRow + 1                   'Increment the counter
    Loop
    'Close the file
    dataWB.Close SaveChanges:=False
    Call Unmerge_And_AutoFit_All(mainSht.Name)
Exit Function
ErrorHandler:
    Application.ScreenUpdating = True
    Application.DisplayAlerts = True
    PrepareSheet_ExcessSickLeave = "ERROR: Failed to process " & inputFile
End Function

Function PrepareSheet_UnplannedLeave(mainWB As Workbook, inputFile As String, templateSheet As String) As String
'Prepares the data in the Excess Rec Leave and adds it to the GM Dashboard monthly report template.
    'Data workbook variables
    Dim dataWB As Workbook
    Dim dataSht As Worksheet
    Dim dataRow As Integer, dataLastRow As Integer
    Dim businessAreaName As String

    On Error GoTo ErrorHandler:
    Application.DisplayAlerts = False
    Set dataWB = Workbooks.Open(inputFile)
    Application.DisplayAlerts = True

    Set dataSht = dataWB.Sheets(1)
    Call Unmerge_And_AutoFit_All(dataSht.Name)
    Call RemoveNewLineCharacters(dataWB, dataSht.Name)
    Dim col_lookupCriteria As Double
    Dim lookupCriteria(10, 1) As String
    Dim counter As Integer
    Dim arrayLength As Integer
    'Array structure:
    '[criteria name][relevant column number in the data sheet]
    'Insert criteria names into the array
    lookupCriteria(0, 0) = "Sick Leave Days"
    lookupCriteria(1, 0) = "Sick Leave Occ"
    lookupCriteria(2, 0) = "<1 Day Rate"
    lookupCriteria(3, 0) = "1 Day Rate"
    lookupCriteria(4, 0) = "1-3 Day Rate"
    lookupCriteria(5, 0) = "3-5 Day Rate"
    lookupCriteria(6, 0) = "> 5 Day Rate"
    lookupCriteria(7, 0) = "HR Headcount"
    lookupCriteria(8, 0) = "Avge Sick Leave Days"
    lookupCriteria(9, 0) = "Avge Sick Leave Occ"
    lookupCriteria(10, 0) = "Absence Rate"
    'Get the relevantcolumn number from the dataSht for each crtieria name
    counter = 0
    arrayLength = arrayLen(lookupCriteria)
    While counter < arrayLength
        lookupCriteria(counter, 1) = GetColumnLetter(WorksheetFunction.Match(lookupCriteria(counter, 0), dataSht.Rows(1), 0))
        counter = counter + 1
    Wend
    'Template workbook variables
    Dim mainSht As Worksheet
    Dim mainRng As Range
    Dim mainRow As Integer, mainLastRow As Integer
    Set mainSht = mainWB.Sheets(templateSheet)
    mainLastRow = mainSht.UsedRange.Rows.Count
    If mainLastRow < 3 Then
        mainLastRow = 3
    Else
    End If
    Set mainRng = mainSht.Range("A3:C" & mainLastRow)
    mainRng.EntireRow.Delete
    mainRow = 3
    Call RemoveNewLineCharacters(mainWB, templateSheet)
    'Loop through data sheet and sum totals
    dataRow = 3
    dataLastRow = dataSht.UsedRange.Rows.Count
    Dim prevBusinessAreaName As String
    Dim col_OrgUnits As Integer
    col_OrgUnits = WorksheetFunction.Match("Org Unit Hier Lvl 4", dataSht.Rows(2), 0)
    Dim SPP_Done As Boolean
    SPP_Done = False
    Do While dataRow <= dataLastRow
        If prevBusinessAreaName = "Strategic Planning & Performance" Then
            SPP_Done = True
            dataRow = dataRow - 1
        End If
        businessAreaName = dataSht.Cells(dataRow, col_OrgUnits)
        If businessAreaName = "Strategic Planning & Performance" And SPP_Done = True Then
            dataSht.Cells(dataRow, col_OrgUnits).value = dataSht.Cells(dataRow, col_OrgUnits + 1).value
            businessAreaName = dataSht.Cells(dataRow, col_OrgUnits)
        End If
        'If the current row is not the same businessAreaName as the previous row
        If Not businessAreaName = prevBusinessAreaName Then
            'Get values from data sheet
            counter = 0
            mainSht.Range("A" & mainRow).value = businessAreaName
            While counter < arrayLength
                Dim lookingFor As String
                Dim lookupCol As String
                lookingFor = lookupCriteria(counter, 0)
                lookupCol = lookupCriteria(counter, 1)
                'get correct main sheet column
                Dim mainCol As String
                mainCol = GetColumnLetter(WorksheetFunction.Match(lookingFor, mainSht.Rows(1), 0))
                If lookingFor = "Sick Leave Occ" Or lookingFor = "HR Headcount" Then
                    mainSht.Range(mainCol & mainRow).value = Format(WorksheetFunction.SumIf(dataSht.Columns(col_OrgUnits), businessAreaName, dataSht.Columns(lookupCol)), "0")
                Else
                    If lookingFor = "Absence Rate" Then
                        Dim leaveHoursCol As String
                        Dim plannedHoursCol As String
                        leaveHoursCol = GetColumnLetter(WorksheetFunction.Match("Leave Hours", dataSht.Rows(1), 0))
                        plannedHoursCol = GetColumnLetter(WorksheetFunction.Match("Planned Working Hrs", dataSht.Rows(1), 0))
                        mainSht.Range(mainCol & mainRow).value = Format(WorksheetFunction.SumIf(dataSht.Columns(col_OrgUnits), businessAreaName, dataSht.Columns(leaveHoursCol)) / WorksheetFunction.SumIf(dataSht.Columns(col_OrgUnits), businessAreaName, dataSht.Columns(plannedHoursCol)), "0%")
                    Else
                        mainSht.Range(mainCol & mainRow).value = Format(WorksheetFunction.SumIf(dataSht.Columns(col_OrgUnits), businessAreaName, dataSht.Columns(lookupCol)), "0.00")
                    End If
                End If
                counter = counter + 1
            Wend
            'Colour the cells appropriately
            With mainSht
                .Range("A" & mainRow).Interior.Color = RGB(198, 196, 196)
                'If odd numbered row, set cell colour to Blue
                If Not mainRow Mod 2 = 0 Then
                    .Range("B" & mainRow & ":L" & mainRow).Interior.Color = RGB(221, 235, 247)
                Else: End If
                'Apply a border to the new data
                With .Range("A" & mainRow & ":L" & mainRow).Borders
                    .LineStyle = xlContinuous
                    .Color = RGB(166, 166, 166)
                    .Weight = xlMedium
                End With
            End With
            mainRow = mainRow + 1 'Increment the main workbook row count
        Else
            'Clear values and check next row
            GoTo ContinueDo:
        End If
ContinueDo:
        prevBusinessAreaName = businessAreaName
        dataRow = dataRow + 1 'Increment the data workbook row count
    Loop
    'Close the file
    dataWB.Close SaveChanges:=False
    Call Unmerge_And_AutoFit_All(mainSht.Name)
    Exit Function
ErrorHandler:
    Application.ScreenUpdating = True
    Application.DisplayAlerts = True
    PrepareSheet_UnplannedLeave = "ERROR: Unable to process " & inputFile
End Function

Function PrepareSheet_EPP(mainWB As Workbook, inputFile As String, templateSheet As String) As String
'Prepares the data in the Excess Rec Leave and adds it to the GM Dashboard monthly report template.
    'Data workbook variables
    Dim dataWB As Workbook
    Dim dataSht As Worksheet
    Dim dataRow As Integer, dataLastRow As Integer
    Dim businessAreaName As String

    On Error GoTo ErrorHandler:
    Application.DisplayAlerts = False
    Set dataWB = Workbooks.Open(inputFile)
    Application.DisplayAlerts = True

    Set dataSht = dataWB.Sheets(1)
    'Template workbook variables
    Dim mainSht As Worksheet
    Dim mainRng As Range
    Dim mainRow As Integer, mainLastRow As Integer
    Set mainSht = mainWB.Sheets(templateSheet)
    mainLastRow = mainSht.UsedRange.Rows.Count
    If mainLastRow < 3 Then
        mainLastRow = 3
    Else
    End If
    Set mainRng = mainSht.Range("A3:C" & mainLastRow)
    mainRng.EntireRow.Delete
    mainRow = 3
    'Loop through data sheet and sum totals
    dataRow = 3
    dataLastRow = dataSht.UsedRange.Rows.Count
    Dim prevBusinessAreaName As String
    Call Unmerge_And_AutoFit_All(dataSht.Name)
    'SumIf columns
    Dim col_EPPCount As Integer
    Dim col_HRHeadCount As Integer
    Dim col_EPPNotCurrent As Integer
    Dim col_OrgUnits As Integer
    col_OrgUnits = WorksheetFunction.Match("Org Unit Hier Lvl 4", dataSht.Rows(2), 0)
    col_EPPCount = WorksheetFunction.Match("Current EPP Count", dataSht.Rows(1), 1)
    col_HRHeadCount = WorksheetFunction.Match("HR Headcount", dataSht.Rows(1), 0)
    col_EPPNotCurrent = WorksheetFunction.Match("EPP not Current Count", dataSht.Rows(1), 1)
    Dim EPPRate As Double
    Dim EPPNotCurrent As Long
    Dim SPP_Done As Boolean
    SPP_Done = False
    Do While dataRow <= dataLastRow
        If prevBusinessAreaName = "Strategic Planning & Performance" Then
            SPP_Done = True
            dataRow = dataRow - 1
        End If
        businessAreaName = dataSht.Cells(dataRow, col_OrgUnits)
        If businessAreaName = "Strategic Planning & Performance" And SPP_Done = True Then
            dataSht.Cells(dataRow, col_OrgUnits).value = dataSht.Cells(dataRow, col_OrgUnits + 1).value
            businessAreaName = dataSht.Cells(dataRow, col_OrgUnits)
        End If
        'If the current row is not the same businessAreaName as the previous row
        If Not businessAreaName = prevBusinessAreaName Then
            With dataSht
                'Get the EPP Rate and Not Current Count values
                EPPRate = WorksheetFunction.SumIf(dataSht.Columns(col_OrgUnits), businessAreaName, dataSht.Columns(col_EPPCount)) / _
                          WorksheetFunction.SumIf(dataSht.Columns(col_OrgUnits), businessAreaName, dataSht.Columns(col_HRHeadCount))
                EPPNotCurrent = WorksheetFunction.SumIf(dataSht.Columns(col_OrgUnits), businessAreaName, dataSht.Columns(col_EPPNotCurrent))
            End With
            With mainSht
                'Write the values to the main workbook template
                .Range("A" & mainRow).value = businessAreaName
                .Range("B" & mainRow).value = Format(EPPRate, "0%")
                .Range("C" & mainRow).value = EPPNotCurrent
                'Colour the cells appropriately
                .Range("A" & mainRow).Interior.Color = RGB(198, 196, 196)
                'If odd numbered row, set cell colour to Blue
                If Not mainRow Mod 2 = 0 Then
                    .Range("B" & mainRow & ":C" & mainRow).Interior.Color = RGB(221, 235, 247)
                Else: End If
                'Apply a border to the new data
                With .Range("A" & mainRow & ":C" & mainRow).Borders
                    .LineStyle = xlContinuous
                    .Color = RGB(166, 166, 166)
                    .Weight = xlMedium
                End With
            End With
            mainRow = mainRow + 1 'Increment the main workbook row count
        Else
            'Clear values and check next row
            EPPRate = 0
            EPPNotCurrent = 0
            GoTo ContinueDo:
        End If
ContinueDo:
        prevBusinessAreaName = businessAreaName
        dataRow = dataRow + 1 'Increment the data workbook row count
    Loop
    'Close the file
    dataWB.Close SaveChanges:=False
    Call Unmerge_And_AutoFit_All(mainSht.Name)
Exit Function
ErrorHandler:
    Application.ScreenUpdating = True
    Application.DisplayAlerts = True
    PrepareSheet_EPP = "ERROR: Unable to process " & inputFile
End Function

Function PrepareSheet_Accelerate(mainWB As Workbook, reportDirectory As String, filenameCriteria As String, templateSheet As String) As String
'Prepares the data in the Excess Rec Leave and adds it to the GM Dashboard monthly report template.
    Dim dataWB As Workbook
    Dim dataSht As Worksheet, mainSht As Worksheet
    Dim dataRng As Range, mainRng As Range, mainColA As Range, mainRow1 As Range
    Dim StrFile As String
    On Error GoTo ErrorHandler:
    Application.DisplayAlerts = False
    StrFile = Dir(reportDirectory & "\*" & filenameCriteria)
    'For each file in the directory matching the filename criteria
    Set mainSht = mainWB.Sheets(templateSheet)
    Set mainRng = mainSht.UsedRange
    Set mainColA = mainSht.Range("A:A")
    Set mainRow1 = mainSht.Range("1:1")
    'Loop through each file in the direcotry
    Do While Len(StrFile) > 0
        Dim lastRow As Integer, row As Integer
        Dim businessAreaName As Variant
        Dim copyRng As Range, pasteRng As Range
        'Open the file
        Set dataWB = Workbooks.Open(reportDirectory + "\" + StrFile)
        Set dataSht = dataWB.Sheets(1)
        businessAreaName = Split(StrFile, " -", 2)(0)
        Set dataRng = dataSht.UsedRange.Columns(1)
        'Get the number of rows
        lastRow = dataRng.Rows.Count
        row = 2
        'Loop through each row and get the course name and % complete
        Do While row <= lastRow
            Dim courseName As String
            courseName = Cells(row, 1)
            Set copyRng = Cells(row, 2)
            If copyRng.value > 0.99 And copyRng.value < 1 Then
                copyRng.value = 0.99
            Else
            End If
            Set pasteRng = Nothing
            On Error Resume Next
            Set pasteRng = WorksheetFunction.Index(mainRng, WorksheetFunction.Match(businessAreaName, mainColA, 0), WorksheetFunction.Match(courseName, mainRow1, 0))
            If Not pasteRng Is Nothing Then
                pasteRng.value = copyRng.value
            Else
            End If
            row = row + 1
        Loop
        'Close the file
        dataWB.Close SaveChanges:=False
        StrFile = Dir
    Loop
Exit Function
ErrorHandler:
    Application.ScreenUpdating = True
    Application.DisplayAlerts = True
    PrepareSheet_Accelerate = "ERROR: Failed to process the Accelerate reports"
End Function

Function PrepareSheet_RecLeave(mainWB As Workbook, inputFile As String, templateSheet As String) As String
'Prepares the data in the Excess Rec Leave and adds it to the GM Dashboard monthly report template.
    Dim dataWB As Workbook
    Dim dataSht As Worksheet, mainSht As Worksheet
    Dim lastRow As Integer
    'Name of the sheet on the template report
    'Open the downloaded report and copy the entire sheet to the template report
    On Error GoTo ErrorHandler:
    Application.DisplayAlerts = False
    Set dataWB = Workbooks.Open(inputFile)
    Application.DisplayAlerts = True

    'Copy the data from the downloaded report into the consolidated workbook
    Set dataSht = dataWB.Worksheets(1)
    Set mainSht = mainWB.Worksheets(templateSheet)
    dataSht.Copy mainWB.Sheets(1)
    Application.DisplayAlerts = False
    mainWB.Worksheets(templateSheet).Delete
    Application.DisplayAlerts = True
    mainWB.Sheets(1).Name = templateSheet
    Set mainSht = mainWB.Worksheets(templateSheet)
    'Find the row named 'Overall Result' and delete it
    lastRow = mainSht.UsedRange.Rows.Count
    While lastRow > 0
        Dim cellValue As String
        cellValue = mainSht.Cells(lastRow, 1)
        If cellValue = "Overall Result" Then
            mainSht.Rows(lastRow).EntireRow.Delete
            lastRow = 0
        Else 'Do nothing
        lastRow = lastRow - 1
        End If
    Wend
    'Unmerge all cells and autofit columns and rows
    Call Unmerge_And_AutoFit_All(templateSheet)
    'Rearrange columns per report requirements
    lastRow = mainSht.UsedRange.Rows.Count
    Dim orgUnitTitle As String
    Dim colNo As Long
    Dim orgUnitLevel As Integer
    Dim colX As String
    orgUnitLevel = 5
    orgUnitTitle = "Org Unit Hier Lvl " & orgUnitLevel
    While orgUnitLevel >= 3
        'Find org unit level 3 column
        colNo = Application.WorksheetFunction.Match(orgUnitTitle, Rows(3), 0)
        colX = GetColumnLetter(colNo)
        Range(colX + "3" + ":" + colX + CStr(lastRow)).Cut
        Range("A3").Insert Shift:=xlToRight
        orgUnitLevel = orgUnitLevel - 1
        orgUnitTitle = "Org Unit Hier Lvl " & orgUnitLevel
    Wend
    'Delete Org Unit level 3 column
    Columns(1).Delete
    'Move Organisational Unit columns before Employee columns
    colNo = Application.WorksheetFunction.Match("Organisational Unit", Rows(3), 0)
    colX = GetColumnLetter(colNo)
    Range(colX + "3" + ":" + GetColumnLetter(colNo + 1) + CStr(lastRow)).Cut
    colNo = Application.WorksheetFunction.Match("Employee", Rows(3), 0)
    colX = GetColumnLetter(colNo)
    Range(colX & "3").Insert Shift:=xlToRight
    Call AutoFitAll(templateSheet)
    'Close the downloaded report without saving
    dataWB.Close SaveChanges:=False
Exit Function
ErrorHandler:
    Application.ScreenUpdating = True
    Application.DisplayAlerts = True
    PrepareSheet_RecLeave = "ERROR: Unable to process " & inputFile
End Function

Function SaveAndCloseTemplate(mainWB As Workbook)
'Save and close the main workbook file
    Application.DisplayAlerts = False
    mainWB.Close SaveChanges:=True
    Application.DisplayAlerts = True
End Function

Function CopyTemplateFile(inFilePath As String, inFileName As String) As Workbook
'Create a new copy of the GM Dashboard HR Report - XXXX where X is the current month, i.e. August
    Dim newMonthFolderName As String
    Dim currentMonth As String, currentYear As String
    Dim outFilePath As String
    outFilePath = inFilePath + "test\" 'remove & test in production 
    currentMonth = Format(Date, "mmmm")
    currentYear = Format(Date, "yyyy")
    newMonthFolderName = Format(Date, "m") + ". " + currentMonth
    'Check if the relevant YEAR folder exists
    If Not FolderExists(outFilePath + currentYear) Then
        'If not, create YEAR folder
        MkDir (outFilePath + currentYear)
        outFilePath = outFilePath + currentYear + "\"
        MkDir (outFilePath + newMonthFolderName)
        outFilePath = outFilePath + "\" + newMonthFolderName
    Else 'The YEAR folder already exists, so look for MONTH folder
        If Not FolderExists(outFilePath + currentYear + "\" + newMonthFolderName) Then
            'If not MONTH folder, create MONTH folder
            MkDir (outFilePath + currentYear + "\" + newMonthFolderName)
            outFilePath = outFilePath + currentYear + "\" + newMonthFolderName
        Else
            'The correct file structure already exists, so delete existing report if any
            outFilePath = outFilePath + currentYear + "\" + newMonthFolderName
            Dim FSO
            Dim sFile As String
            'Source File Location
            sFile = outFilePath + "\GM Dashboard - " + currentMonth + " " + currentYear + ".xlsx"
            'Set Object
            Set FSO = CreateObject("Scripting.FileSystemObject")
            'Check File Exists or Not
            If FSO.FileExists(sFile) Then
                'If file exists, It will delete the file from source location
                FSO.DeleteFile sFile, True
            Else
            End If
        End If
    End If
    Set CopyTemplateFile = ActiveWorkbook
    CopyTemplateFile.SaveAs fileName:=outFilePath + "\GM Dashboard - " + currentMonth + " " + currentYear + ".xlsx"
End Function

Function Unmerge_And_AutoFit_All(Optional sheet As String) As String
'Auto fits all columns and rows and unmerges all cells on all sheets
'Accepts a String as Optional argument.  If supplied, code will execute on that sheet
'only, otherwise the code will loop through all sheets in the workbook
    Dim sht As Worksheet
    Dim rng As Range
    Dim msg As String
    'If no sheet name supplied, loop through all sheets
    If sheet = "" Then
        'Execute on all sheets in workbook
        On Error GoTo ErrorHandler
        For Each sht In Worksheets
            Set rng = sht.UsedRange
            rng.UnMerge
            rng.Cells.WrapText = False
            rng.Columns.AutoFit
            rng.Rows.AutoFit
        Next sht
        'Return success message
        Unmerge_And_AutoFit_All = msg & "VBA: Code executed successfully"
        Exit Function
    Else
        'Otherwise, only execute on the sheet name supplied
        On Error GoTo ErrorHandler
        Set sht = ActiveWorkbook.Worksheets(sheet)
        Set rng = sht.UsedRange
        rng.UnMerge
        rng.Cells.WrapText = False
        rng.Columns.AutoFit
        rng.Rows.AutoFit
    'Return success message
    Unmerge_And_AutoFit_All = msg & "VBA: Code executed successfully"
    End If
    Exit Function
ErrorHandler:
    'Return error message
    Unmerge_And_AutoFit_All = msg & "ERROR: The Unmerge and AutoFit VBA code failed to execute"
End Function

Function GetColumnLetter(col As Long) As String
    Dim vArr
    vArr = Split(Cells(1, col).Address(True, False), "$")
    GetColumnLetter = vArr(0)
End Function

Sub AutoFitAll(sheetName As String)
'Auto fits all columns and rows on all sheets'
    Dim sht As Worksheet
    Set sht = Worksheets(sheetName)
    sht.UsedRange.Columns.AutoFit
    sht.UsedRange.Rows.AutoFit
End Sub

Sub RemoveNewLineCharacters(wb As Workbook, sheetName As String)
    Dim rng As Range
    Dim sht As Worksheet
    Set sht = wb.Worksheets(sheetName)
    Application.Calculation = xlCalculationManual
    For Each rng In sht.UsedRange
        If 0 < InStr(rng, Chr(10)) Then
            rng = replace(rng, Chr(10), " ")
            rng = replace(rng, "  ", " ")
        End If
    Next
    Application.Calculation = xlCalculationAutomatic
End Sub

Function arrayLen(arr As Variant) As Integer
'Returns the length of the array passed to it
    arrayLen = UBound(arr) - LBound(arr) + 1
End Function

Function FolderExists(filePath As String) As Boolean
    If filePath = vbNullString Then Exit Function
    On Error GoTo EarlyExit
    If Not Dir(filePath, vbDirectory) = vbNullString Then FolderExists = True
EarlyExit:
    On Error GoTo 0
End Function