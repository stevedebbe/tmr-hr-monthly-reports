'
' VBA code and functions to support the HR Reporting UiPath Bot
' The purpose of the code in this file is to populate the monthly GM Dashboard Word template
' with the data in the GM Dashboard Excel file.
' Author: Matthew Hodgson (matt.hodgson@blackbook.ai)
' Date: 31 August 2018
'
Option Explicit

Function Main(inFilePath As String, inFileName As String, inConfigFilePath As String)
'inFilePath: Folder directory containing the GM Dashboard HR Data word template
'inFileName: Name of the GM Dashboard HR Data word template
'Creates a new copy of the GM Dashboard HR Data word template and populates it with data from
'the current month's GM Dashboard excel file 
    Dim wordDoc As Object
    Set wordDoc = CopyTemplateFile(inFilePath, inFileName)
    'Update the word template Title and Subtitles with current date information
    Call UpdateReportHeadings(wordDoc)
    'Update tables in the word document template with data from the GM Dashboard spreadsheet
    Call UpdateTableData(wordDoc, "Table_ExcessRecLeave", "Excess Rec Leave", inConfigFilePath)
    Call UpdateTableData(wordDoc, "Table_AccelerateCourseCompliance", "Accelerate", inConfigFilePath)
    Call UpdateTableData(wordDoc, "Table_PDACompletion", "PDA Stats", inConfigFilePath)
    'Update the Gender Diversity table (uses different logic to the above tables)
    Call UpdateTableData_GenderDiversity(wordDoc, "Table_GenderDiversity", "Gender")
    'Save and close the word document template
    Call SaveAndCloseDocument(wordDoc)
    Call CloseWord
End Function

Function CopyTemplateFile(inFilePath As String, inFileName As String) As Object
'inFilePath: Folder directory containing the GM Dashboard HR Data work template
'inFileName: Name of the GM Dashboard HR Data work template
'Create a new copy of the GM Dashboard HR Report - XXXX where X is the current month, i.e. August
    Dim wordApp As Object
    Set wordApp = CreateObject("Word.Application")
    wordApp.Visible = False
    Dim newMonthFolderName As String
    Dim filePath As String
    filePath = inFilePath & "test\" 'remove & test in production 
    Dim currentMonth As String
    Dim currentYear As String
    currentMonth = Format(Date, "mmmm")
    currentYear = Format(Date, "yyyy")
    newMonthFolderName = Format(Date, "m") + ". " + currentMonth
    If Not FolderExists(filePath + currentYear) Then
        'If not, create YEAR folder
        MkDir (filePath + currentYear)
        filePath = filePath + currentYear + "\"
        MkDir (filePath + newMonthFolderName)
        filePath = filePath + "\" + newMonthFolderName
    Else 'The YEAR folder already exists, so look for MONTH folder
        If Not FolderExists(filePath + currentYear + "\" + newMonthFolderName) Then
            'If not MONTH folder, create MONTH folder
            MkDir (filePath + currentYear + "\" + newMonthFolderName)
            filePath = filePath + currentYear + "\" + newMonthFolderName
        Else
            'The correct file structure already exists, so delete existing report if any
            filePath = filePath + currentYear + "\" + newMonthFolderName
            Dim FSO
            Dim sFile As String
            'Source File Location
            sFile = filePath + "\GM Dashboard - " + currentMonth + " " + currentYear + ".xlsx"
            'Set Object
            Set FSO = CreateObject("Scripting.FileSystemObject")
            'Check File Exists or Not
            If FSO.FileExists(sFile) Then
                'If file exists, It will delete the file from source location
                FSO.DeleteFile sFile, True
            Else
            End If
        End If
    End If
    Set CopyTemplateFile = wordApp.Documents.Open(inFilePath + currentYear + "\" + inFileName)
    CopyTemplateFile.SaveAs Filename:=filePath + "\GM Dashboard HR Data - " + currentMonth + " " + currentYear + ".docx"
    'Set wordApp = Nothing
End Function

Function UpdateTableData_GenderDiversity(wordDoc As Object, tableName As String, sheetName As String)
'wordDoc:   The word document object we're working with
'tableName: Name of the table Bookmark name in the GM Dashboard word document template
'sheetName: Name of the sheet in the GM Dashboard excel file that we'll retrieve data from 
'Updates the Gender Diversity table on word document template with the data from the
'Gender Diversity sheet in the GM Dashboard spreadsheet
    Dim sht As Worksheet
    Set sht = Sheets(sheetName)
    Dim colBA As Range, colFemale As Range, colMale As Range
    Set colBA = sht.Range("A:A")
    Set colFemale = sht.Range("B:B")
    Set colMale = sht.Range("C:C")
    Dim lRow As Integer
    lRow = sht.UsedRange.Rows.Count
    Dim businessArea As String, femaleCount As String, maleCount As String, unknownCount As String
    Dim table As Object
    Set table = wordDoc.Bookmarks(tableName).Range.Tables(1)
    While lRow >= 3
        'Get values from spreadsheet
        businessArea = sht.Range("A" & lRow).value
        femaleCount = sht.Range("B" & lRow).value
        maleCount = sht.Range("C" & lRow).value
        unknownCount = sht.Range("D" & lRow).value
        'Populate word template with those values
        lRow = lRow - 1
        Dim rw As Object
        Set rw = wordDoc.Range.Cells
        For Each rw In table.Columns(1).Cells
            If AlphaNumericOnly(rw.Range.Text) = businessArea Then
                table.Cell(rw.RowIndex, 2).Range.Text = femaleCount
                table.Cell(rw.RowIndex, 3).Range.Text = maleCount
                table.Cell(rw.RowIndex, 4).Range.Text = unknownCount
            End If
        Next
    Wend
End Function

Function SaveAndCloseDocument(wordDoc As Object)
'wordDoc: The word document we're working with
'Saves and closes the word document passed as argument
    WordDoc.Save
End Function

Function UpdateTableData(wordDoc As Object, tableName As String, sheetName As String, inConfigFilePath As String)
'wordDoc:   The word document object we're working with
'tableName: Name of the table Bookmark name in the GM Dashboard word document template
'sheetName: Name of the sheet in the GM Dashboard excel file that we'll retrieve data from 
'Populates the Excess Leave table with data from the Excess Rec Leave tab in the GM Dashboard Excel file
    'Variables
    Dim sht As Worksheet
    Dim colA, colB As Range
    Dim counter As Integer, arrayLength As Integer
    Dim idsp As Integer, idbs As Integer, idrp As Integer, enablingUnitsTotal As Integer
    Dim countOfBusinessArea As String, businessAreaName As String, businessAreaAltName As String
    Dim dotColour As String 
    counter = 0
    'businessAreas array holds the business area and alternative business area names
    'These are the names that the business areas are referred to in the word and excel templates
    'If you need to add a new business area to the reports, then 
    '--------------------------------------------------
    Dim configWB As Workbook
    Set configWB = Workbooks.Open(inConfigFilePath & "Config_BusinessAreaList.xlsx")
    Dim configSht As Worksheet
    Set configSht = configWB.Worksheets(1)
    Dim configRw As Integer
    Dim configRws As Integer 
    configRws = configSht.UsedRange.Rows.Count
    configRw = 0
    Dim configRng As Range
    Set configRng = configSht.Range("A1:B" & configRws)
    '-------------------------------------------------
    Dim businessAreas() As String '-2 for header and because array starts from 0 not 1
    configRws = configRws - 1
    ReDim businessAreas(configRws, 1) '-2 for header and because array starts from 0 not 1
    configRws = configRws + 1
    Do While configRw <= ConfigRws -1
        businessAreas(configRw, 0) = configSht.Range("A" & configRw+1).value
        businessAreas(configRw, 1) = configSht.Range("B" & configRw+1).value
        configRw = configRw + 1
    Loop
    configWB.Close SaveChanges:=False
    'To add a new business area, increment the first 
    'number on the line above, then add an additional
    'element to the array as seen in the examples below
    'Increase number of business areas example:
    'Dim businessAreas(10, 1) As String
    '--------------------------------------------------
    'Business Area names
    'businessAreas(0, 0) = "Northern"
    'businessAreas(1, 0) = "Central"
    'businessAreas(2, 0) = "Southern"
    'businessAreas(3, 0) = "SEQ North"
    'businessAreas(4, 0) = "SEQ South"
    'businessAreas(5, 0) = "Central Operations & Support"
    'businessAreas(6, 0) = "Systems, Procedures & Training"
    'businessAreas(7, 0) = "GMO & Business Services"
    'businessAreas(8, 0) = "Research, Planning & Performance"
    'businessAreas(9, 0) = "Call Centre (CS Direct)"
    '--------------------------------------------------
    'New business area name example:
    'businessAreas(10, 0) = "Western"
    '--------------------------------------------------
    'Alternative Business Area names
    'businessAreas(0, 1) = "Northern"
    'businessAreas(1, 1) = "Central"
    'businessAreas(2, 1) = "Southern"
    'businessAreas(3, 1) = "SEQ North"
    'businessAreas(4, 1) = "SEQ South"
    'businessAreas(5, 1) = "CO&S"
    'businessAreas(6, 1) = "SP&T"
    'businessAreas(7, 1) = "BServ"
    'businessAreas(8, 1) = "RP&P"
    'businessAreas(9, 1) = "CS Direct"
    '--------------------------------------------------
    'New alternative business area name example:
    'businessAreas(10, 1) = "West"
    '--------------------------------------------------
    'Get the length of the array
    arrayLength = arrayLen(businessAreas)
    'Loop through the array of business area names
    While counter < arrayLength
        'Set current BusinessArea values (Name, ID, AltName)
        businessAreaName = businessAreas(counter, 0)
        businessAreaAltName = businessAreas(counter, 1)
        'Depending on the sheetName passed as argument, there are differing methods to
        'retrieve the relevant values from the GM Dashboard Excel file. The Cases below
        'ensure that the correct method to retrieve the values on each sheet are executed
        Select Case sheetName
            Case "Excess Rec Leave"
                countOfBusinessArea = Retrieve_ExcessRecLeave(sheetName, businessAreaName)
            Case "Accelerate"
                countOfBusinessArea = Retrieve_Accelerate(sheetName, businessAreaName)
            Case "PDA Stats"
                countOfBusinessArea = Retrieve_PDACompletion(sheetName, businessAreaName)
            Case Else
                Exit Function
        End Select
        'Update template value
        'Call ReplaceTableText(wordDoc, tableName, businessAreas(Counter, 1), countOfBusinessArea)
        Call ReplaceTableTextLoop(wordDoc, tableName, businessAreaName, countOfBusinessArea, businessAreaAltName)
        'Don't update dots for id-sp, id-bs, id-rp, however roll the values up into enablingUnitsTotal
        If businessAreaName = "Systems, Procedures & Training" Then
            idsp = countOfBusinessArea
        Else
            If businessAreaName = "GMO & Business Services" Then
                idbs = countOfBusinessArea
            Else
                If businessAreaName = "Research, Planning & Performance" Then
                    idrp = countOfBusinessArea
                Else
                    'Determine the dot colour
                    dotColour = DetermineDotColour(wordDoc, tableName, countOfBusinessArea)
                    'Update the dot colour
                    Call UpdateDotColourLoop(wordDoc, tableName, businessAreaName, businessAreaAltName, dotColour, inConfigFilePath)
                End If
            End If
        End If
        counter = counter + 1
    Wend
    ' Total
    If tableName = "Table_AccelerateCourseCompliance" Or _
       tableName = "Table_PDACompletion" Then
       'Calculate average for enablingUnitsTotal
        enablingUnitsTotal = (idsp + idbs + idrp) / 3
    Else
        'Calculate sum total for enablingUnitsTotal
        enablingUnitsTotal = idsp + idbs + idrp
    End If
    'Call ReplaceTableText(wordDoc, tableName, "id-eu", CStr(enablingUnitsTotal))
    businessAreaName = "Enabling Units"
    businessAreaAltName = "Enabling Units"
    Call ReplaceTableTextLoop(wordDoc, tableName, businessAreaName, CStr(enablingUnitsTotal), businessAreaAltName)
    dotColour = DetermineDotColour(wordDoc, tableName, CStr(enablingUnitsTotal))
    Call UpdateDotColourLoop(wordDoc, tableName, businessAreaName, businessAreaAltName, dotColour, inConfigFilePath)
End Function

Function UpdateReportHeadings(wordDoc As Object)
'wordDoc:   The word document object we're working with 
'Updates the ReportTitle and Headings
    'Get current day, month and year values
    Dim currentDay As String
    Dim currentMonth As String
    Dim currentYear As String
    currentMonth = Format(Date, "mmmm")
    currentYear = Format(Date, "yyyy")
    currentDay = Format(Date, "d")
    'Replace text in the word document with the current date values
    Call ReplaceText(wordDoc, "titleMonth", currentMonth)
    Call ReplaceText(wordDoc, "titleYear", currentYear)
    Call ReplaceText(wordDoc, "currentDay", currentDay)
    Call ReplaceText(wordDoc, "currentMonth", currentMonth)
    Call ReplaceText(wordDoc, "currentYear", currentYear)
End Function

Function FolderExists(filePath As String) As Boolean
'filePath: The folder path to check if  
    If filePath = vbNullString Then Exit Function
    On Error GoTo EarlyExit
    If Not Dir(filePath, vbDirectory) = vbNullString Then FolderExists = True
EarlyExit:
    On Error GoTo 0
End Function

Function ReplaceText(wordDoc As Object, oldText As String, newText As String)
'wordDoc:   The word document object we're working with
'oldText:   The string to search for in the document 
'newText:   The string to replace the 'oldText' with in the document
'Replaces the findText in the Word Document with the replacementText
    'Execute find and replace
    Const wdFindStop As Long = 0
    Const wdReplaceOne As Long = 1
    With wordDoc.Range.Find
        .Text = oldText
        .Replacement.Text = newText
        .Wrap = wdFindStop
        .Execute Replace:=wdReplaceOne
    End With
End Function

Function arrayLen(arr As Variant) As Integer
'arr: The array to get the length of
'Returns the length of the array passed to it
    arrayLen = UBound(arr) - LBound(arr) + 1
End Function

Function Retrieve_ExcessRecLeave(sheetName As String, businessArea As String) As String
'sheetName:     Name of the sheet in the GM Dashboard excel file that we'll retrieve data from 
'businessArea:  Name of the business area we want to retrieve data for
'Retrieve count of the busines area from the Excess Rec Leave sheet
    Dim sht As Worksheet
    Set sht = Sheets(sheetName)
    Dim colA As Range
    Dim colB As Range
    Set colA = sht.Range("A:A")
    Set colB = sht.Range("B:B")
    Dim countOfBusinessArea As String
    countOfBusinessArea = WorksheetFunction.CountIf(colA, businessArea)
    'If the count returns 0, count Column B
    If countOfBusinessArea = 0 Then
        countOfBusinessArea = WorksheetFunction.CountIf(colB, businessArea)
    End If
    Retrieve_ExcessRecLeave = countOfBusinessArea
End Function

Function Retrieve_Accelerate(sheetName As String, businessAreaName As String) As String
'sheetName:     Name of the sheet in the GM Dashboard excel file that we'll retrieve data from 
'businessArea:  Name of the business area we want to retrieve data for
'Retrieve count of the busines area from the Excess Rec Leave sheet
    Dim sht As Worksheet
    Set sht = Sheets(sheetName)
    Dim colA As Range
    Dim colB As Range
    Set colA = sht.Range("I:I")
    Set colB = sht.Range("A:A")
    Dim countOfBusinessArea As String
    'Lookup the businessAreaName'
    On Error GoTo ErrorHandler:
    countOfBusinessArea = WorksheetFunction.Index(colA, WorksheetFunction.Match(businessAreaName, colB, 0), 0)
    Retrieve_Accelerate = Round(Left(countOfBusinessArea, 6) * 100, 0)
    Exit Function
ErrorHandler:
    Retrieve_Accelerate = "0"
End Function

Function Retrieve_PDACompletion(sheetName As String, businessArea As String) As String
'sheetName:     Name of the sheet in the GM Dashboard excel file that we'll retrieve data from 
'businessArea:  Name of the business area we want to retrieve data for
'Retrieve count of the busines area from the Excess Rec Leave sheet
    Dim sht As Worksheet
    Set sht = Sheets(sheetName)
    Dim colA As Range
    Dim colB As Range
    Set colA = sht.Range("B:B")
    Set colB = sht.Range("A:A")
    Dim countOfBusinessArea As String
    On Error GoTo ErrorHandler:
    countOfBusinessArea = WorksheetFunction.Index(colA, WorksheetFunction.Match(businessArea, colB, 0), 0)
    Retrieve_PDACompletion = Round(Left(countOfBusinessArea, 6) * 100, 0)
    Exit Function
ErrorHandler:
    Retrieve_PDACompletion = "0"
End Function

Function ReplaceTableTextLoop(wordDoc As Object, tableName As String, businessAreaName As String, value As String, businessAreaAltName As String)
'wordDoc: The word document object we're working with
'tableName: Name of the table Bookmark name in the GM Dashboard word document template
'businessAreaName: Name of the business area we want to update
'value: The value we want to insert into the word document template
'businessAreaAltName: Alternative name of the business area we want to update
'Replaces the findText in the Word Document with the replacementText
    Dim table As Object
    Set table = wordDoc.Bookmarks(tableName).Range.Tables(1)
    'Execute find and replace
    Dim counter As Integer
    Dim lRow As Integer
    counter = 1
    lRow = table.Rows.Count - 1
    While counter < lRow
        Dim currentRowValue As String
        'Check SPT, BSER and RPP first
        currentRowValue = AlphaNumericOnly(table.Cell(counter + 2, 4).Range.Text)
        If currentRowValue = businessAreaAltName Then
            table.Cell(counter + 2, 5).Range.Text = value
        Else
            currentRowValue = AlphaNumericOnly(table.Cell(counter, 0).Range.Text)
            If currentRowValue = businessAreaName Then
                table.Cell(counter, 2).Range.Text = value
            Else
                If currentRowValue = businessAreaAltName Then
                    table.Cell(counter, 2).Range.Text = value
                Else
                End If
            End If
        End If
        counter = counter + 1
    Wend
End Function

Function DetermineDotColour(wordDoc As Object, tableName As String, cellValueInput As String) As String
'wordDoc: The word document object we're working with
'tableName: Name of the table Bookmark name in the GM Dashboard word document template
'cellValueInput: The string value of the cell value to be used when determining the dot colour
'Compares the cellValueInput against the criteria in the tableName of the wordDoc and returns a dot colour
    'Variables
    Dim table As Object
    Set table = wordDoc.Bookmarks(tableName).Range.Tables(1)
    Dim result As String
    Dim cellValue As Integer
    cellValue = GetNumeric(cellValueInput)
    Dim lowPointStr As String, highPointStr As String
    Dim lowPointInt As Integer, highPointInt As Integer
    lowPointStr = table.Cell(1, 2).Range.Text
    highPointStr = table.Cell(1, 8).Range.Text
    lowPointInt = GetNumeric(lowPointStr)
    highPointInt = GetNumeric(highPointStr)
    result = "yellow"
    'If we're processing the Accelerate or PDA Completion tabs, reverse the dot colour logic
    If tableName = "Table_AccelerateCourseCompliance" Or _
       tableName = "Table_PDACompletion" Then
        If cellValue >= lowPointInt Then
            result = "green"
        Else
            If cellValue <= highPointInt Then
                result = "red"
            End If
        End If
    Else
        'Otherwise, standard logic for dot colours
        If cellValue >= highPointInt Then
            result = "red"
        Else
            If cellValue <= lowPointInt Then
                result = "green"
            End If
        End If
    End If
    DetermineDotColour = result
End Function

Function UpdateDotColourLoop(wordDoc As Object, tableName As String, businessAreaName As String, businessAreaAltName As String, colour As String, pictureDirectory As String)
'wordDoc: The word document object we're working with
'tableName: Name of the table Bookmark name in the GM Dashboard word document template
'businessAreaName: Name of the business area that you want to update the dot for
'businessAreaAltName: Alternative name of the business area that you want to update the dot for
'colour: Colour that the dot should be updated to. Valid options are red, green or yellow
'Loops through each row in the tableName of the wordDoc, finds the businessAreaName and inserts a dot with the colour   
    Dim b As Object
    Set b = wordDoc.Bookmarks(tableName)
    Dim table As Object
    Set table = wordDoc.Bookmarks(tableName).Range.Tables(1)
    'Execute find and replace
    Dim counter As Integer, lRow As Integer
    counter = 1
    lRow = table.Rows.Count - 1
    While counter < lRow
        Dim currentRowValue As String
        currentRowValue = AlphaNumericOnly(table.Cell(counter, 0).Range.Text)
        If currentRowValue = businessAreaName Then
            table.Cell(counter, 3).Range.InlineShapes.AddPicture pictureDirectory + colour + ".png"
        Else
            If currentRowValue = businessAreaAltName Then
                table.Cell(counter, 3).Range.InlineShapes.AddPicture pictureDirectory + colour + ".png"
            End If
        End If
        counter = counter + 1
    Wend
End Function

Function AlphaNumericOnly(inputString As String) As String
'inputString: The string you want to convert to alpha numeric characters only
'Accepts are string as input and returns the string with alpha numeric, '&()' and space characters only
    Dim i As Integer
    Dim strResult As String
    For i = 1 To Len(inputString)
        Select Case Asc(Mid(inputString, i, 1))
            Case 32, 38, 40, 41, 44, 48 To 57, 65 To 90, 97 To 122:  'include 32 if you want to include space
                strResult = strResult & Mid(inputString, i, 1)
        End Select
    Next
    AlphaNumericOnly = strResult
End Function

Function GetNumeric(inputString As String) As Integer
'inputString: The string you want to convert to numeric characters only
'Accepts are string as input and returns the string with numeric characters only
    Dim stringLength As Integer
    Dim i As Integer
    stringLength = Len(inputString)
    Dim result As Integer
    For i = 1 To stringLength
        If IsNumeric(Mid(inputString, i, 1)) Then result = result & Mid(inputString, i, 1)
    Next i
    GetNumeric = result
End Function

Function CloseWord()
'Closes any instance of the Word Application
    Dim obj As Object
    Do
        On Error Resume Next
        Set obj = GetObject(, "Word.Application")
        If Not obj Is Nothing Then
            obj.Quit
            Set obj = Nothing
        End If
    Loop Until obj Is Nothing
End Function